package com.mobimeo.service

import com.mobimeo.genInfo
import com.mobimeo.log
import com.mobimeo.repository.DelayRepository
import com.mobimeo.repository.LineRepository
import com.mobimeo.repository.Location
import com.mobimeo.repository.StopRepository
import com.mobimeo.repository.TimeRepository
import org.springframework.stereotype.Service
import java.time.Clock
import java.time.LocalTime
import java.time.ZoneOffset
import java.time.temporal.ChronoUnit

@Service
class LineService(
    private val clock: Clock,
    private val lineRepository: LineRepository,
    private val stopRepository: StopRepository,
    private val timeRepository: TimeRepository,
    private val delayRepository: DelayRepository
) {

    fun findNextLineStoppingAt(stopId: Int): LineResponse {
        val now = LocalTime.ofInstant(clock.instant(), ZoneOffset.UTC)
        log.genInfo(
            "process" to "get_next_line",
            "status" to "started",
            "stop_id" to stopId,
            "time" to now
        )

        val nextLine = timeRepository.findByStopId(stopId)
            .filter { it.stopsAt >= now } // filter stops happening before this time
            .minBy { ChronoUnit.SECONDS.between(now, it.stopsAt) }
            ?: timeRepository.findFirstLineByTimeOfStop(stopId) // if there is no line today, find the first one of tomorrow
            ?: throw StopNotFoundException(stopId)

        return nextLine.let {
            val line = lineRepository.findByLineId(it.lineId)
                ?: throw IllegalStateException("Line ${it.lineId} does not exist")
            val delay = delayRepository.findByLineName(line.lineName)
            LineResponse(it.lineId, line.lineName, it.stopsAt, stopId, delay?.delayInMinutes)
        }
    }

    fun searchByTimeAndLocation(location: Location, time: LocalTime): List<LineResponse> {
        log.genInfo(
            "process" to "search_location_time",
            "status" to "started",
            "location" to location,
            "time" to time
        )

        return stopRepository.findByLocation(location)
            .flatMap { timeRepository.findByStopIdAndTime(it.stopId, time) }
            .map {
                val line = lineRepository.findByLineId(it.lineId)
                    ?: throw IllegalStateException("Line ${it.lineId} does not exist")
                val delay = delayRepository.findByLineName(line.lineName)
                LineResponse(it.lineId, line.lineName, it.stopsAt, it.stopId, delay?.delayInMinutes)
            }
    }
}

class StopNotFoundException(
    stopId: Int,
    override val message: String = "Stop $stopId could not be found"
) : RuntimeException(message)

data class LineResponse(
    val id: Int,
    val name: String,
    val stopsAt: LocalTime,
    val stopId: Int,
    val delayInMinutes: Int?
)
