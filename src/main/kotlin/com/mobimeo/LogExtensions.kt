package com.mobimeo

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*

inline val <reified T> T.log: Logger
    get() = LoggerFactory.getLogger(T::class.java)

fun Logger.genError(e: Throwable, vararg pairs: Pair<String, Any?>) = error(
    pairs.joinToString(", ") { "${it.first}=${it.second}" }, e)

fun Logger.genInfo(vararg pairs: Pair<String, Any?>) = info(
    pairs.joinToString(", ") { "${it.first}=${it.second}" })

fun Logger.genWarn(vararg pairs: Pair<String, Any?>) = warn(
    pairs.joinToString(", ") { "${it.first}=${it.second}" })
