package com.mobimeo.repository

import java.time.LocalTime

class LineRepository(private val lineRecords: Set<LineRecord>) {

    fun findByLineId(lineId: Int) = lineRecords.firstOrNull { it.lineId == lineId }
}

class StopRepository(private val stopRecords: Set<StopRecord>) {

    fun findByLocation(location: Location) = stopRecords.filter { it.location == location }
}

class DelayRepository(private val delayRecords: Set<DelayRecord>) {

    fun findByLineName(lineName: String) = delayRecords.firstOrNull { it.lineName == lineName }
}

class TimeRepository(timeRecords: Set<TimeRecord>) {

    private val timeRecords = timeRecords.sortedBy { it.stopsAt }

    fun findFirstLineByTimeOfStop(stopId: Int) = timeRecords.firstOrNull { it.stopId == stopId }

    fun findByStopId(stopId: Int) = timeRecords.filter { it.stopId == stopId }

    fun findByStopIdAndTime(stopId: Int, time: LocalTime) = timeRecords.filter {
        it.stopsAt == time && it.stopId == stopId
    }
}
