package com.mobimeo.repository

import java.io.File

class CsvFile(fileName: String) {

    private val file = File(javaClass.classLoader.getResource(fileName).toURI())

    fun readCsv(): List<List<String>> = file.readLines().map { it.split(",") }
}
