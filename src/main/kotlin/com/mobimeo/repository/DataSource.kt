package com.mobimeo.repository

import java.time.LocalTime
import java.time.format.DateTimeFormatter

class DataSource(
    linesFile: CsvFile,
    delaysFile: CsvFile,
    stopsFile: CsvFile,
    timesFile: CsvFile
) {

    val lines = linesFile.readCsv()
        .drop(1)
        .map { LineRecord(it[0].toInt(), it[1]) }
        .toSet()
    val stops = stopsFile.readCsv()
        .drop(1)
        .map {
            StopRecord(
                it[0].toInt(),
                Location(it[1].toInt(), it[2].toInt())
            )
        }
        .toSet()
    val delays = delaysFile.readCsv()
        .drop(1)
        .map { DelayRecord(it[0], it[1].toInt()) }
        .toSet()
    val times = timesFile.readCsv()
        .drop(1)
        .map {
            TimeRecord(
                it[0].toInt(),
                it[1].toInt(),
                LocalTime.parse(it[2], DateTimeFormatter.ISO_TIME)
            )
        }
        .toSet()
}

data class Location(val x: Int, val y: Int)
data class StopRecord(val stopId: Int, val location: Location)
data class DelayRecord(val lineName: String, val delayInMinutes: Int)
data class LineRecord(val lineId: Int, val lineName: String)
data class TimeRecord(val lineId: Int, val stopId: Int, val stopsAt: LocalTime)
