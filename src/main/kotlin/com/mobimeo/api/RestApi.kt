package com.mobimeo.api

import com.mobimeo.genError
import com.mobimeo.log
import com.mobimeo.repository.Location
import com.mobimeo.service.LineService
import com.mobimeo.service.StopNotFoundException
import io.netty.handler.codec.http.HttpHeaderValues.APPLICATION_JSON
import io.vertx.core.http.HttpHeaders.CONTENT_TYPE
import io.vertx.core.http.HttpServerResponse
import io.vertx.core.json.Json
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.Router
import org.springframework.stereotype.Service
import java.time.LocalTime
import java.time.format.DateTimeFormatter

@Service
class RestApi(val lineService: LineService) {

    // TODO improve input validation
    fun register(router: Router) {
        router.get("/health").handler { context ->
            context.response().statusCode = 200
            context.response().end()
        }

        router.get("/api/lines/next")
            .consumes("application/json")
            .produces("application/json")
            .handler { ctx ->
                val response = ctx.response()
                response.putHeader(CONTENT_TYPE, APPLICATION_JSON)
                response.isChunked = true
                try {
                    val request = ctx.request()
                    val stopId =
                        request.params().get("stop")?.toInt() ?: throw BadRequestException("Missing 'stop' query param")
                    response.statusCode = 200
                    response.write(Json.encode(lineService.findNextLineStoppingAt(stopId))).end()
                } catch (e: Exception) {
                    handleError(response, e)
                }
            }

        router.get("/api/lines/search")
            .consumes("application/json")
            .produces("application/json")
            .handler { ctx ->
                val response = ctx.response()
                response.isChunked = true
                response.putHeader(CONTENT_TYPE, APPLICATION_JSON)
                try {
                    val request = ctx.request()
                    val x = request.params().get("x")?.toInt() ?: throw BadRequestException("Missing 'x' query param")
                    val y = request.params().get("y")?.toInt() ?: throw BadRequestException("Missing 'y' query param")
                    val time = request.params().get("time")
                        ?.let { LocalTime.parse(request.params().get("time"), DateTimeFormatter.ISO_TIME) }
                        ?: throw BadRequestException("Missing 'time' query param")
                    val location = Location(x, y)
                    response.statusCode = 200
                    response.write(Json.encode(lineService.searchByTimeAndLocation(location, time))).end()
                } catch (e: Exception) {
                    handleError(response, e)
                }
            }
    }

    fun handleError(response: HttpServerResponse, exception: Exception) {
        log.genError(exception, "process" to "get_next_line", "status" to "error")
        when (exception) {
            is BadRequestException -> {
                response.statusCode = 400
                response.write(Json.encode(ErrorResponse(exception.message))).end()
            }
            is StopNotFoundException -> {
                response.statusCode = 400
                response.write(Json.encode(ErrorResponse(exception.message))).end()
            }
            else -> {
                response.statusCode = 500
                response.write(Json.encode(ErrorResponse("An unexpected error happened"))).end()
            }
        }
    }
}

data class ErrorResponse(val message: String)

class BadRequestException(override val message: String) : RuntimeException(message)
