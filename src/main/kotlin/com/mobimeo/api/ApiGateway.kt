package com.mobimeo.api

import com.mobimeo.log
import io.vertx.core.AbstractVerticle
import io.vertx.core.http.HttpServerOptions
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.LoggerHandler
import org.springframework.stereotype.Component

@Component
class ApiGateway(val restApi: RestApi) : AbstractVerticle() {

    override fun start() {
        val router = Router.router(vertx)
        router.route().handler(LoggerHandler.create())
        router.route().handler(BodyHandler.create())

        restApi.register(router)

        val port = 8081
        vertx.createHttpServer(HttpServerOptions().setPort(port))
            .requestHandler(router)
            .listen { result ->
                if (result.succeeded()) {
                    log.info("Http server listening on port $port")
                } else {
                    log.error("Failed to start server on port $port", result.cause())
                }
            }
    }
}
