package com.mobimeo.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.time.Clock
import java.time.ZoneOffset
import java.util.TimeZone
import javax.annotation.PostConstruct

@Configuration
class ClockConfig {

    @PostConstruct
    fun setDefaultTimeZone() = TimeZone.setDefault(TimeZone.getTimeZone(ZoneOffset.UTC))

    @Bean
    fun clock() = Clock.systemUTC()
}
