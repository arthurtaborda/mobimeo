package com.mobimeo.config

import com.mobimeo.repository.CsvFile
import com.mobimeo.repository.DataSource
import com.mobimeo.repository.DelayRepository
import com.mobimeo.repository.LineRepository
import com.mobimeo.repository.StopRepository
import com.mobimeo.repository.TimeRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class DataSourceConfig {

    @Bean
    fun dataSource() = DataSource(
        CsvFile("datasource/lines.csv"),
        CsvFile("datasource/delays.csv"),
        CsvFile("datasource/stops.csv"),
        CsvFile("datasource/times.csv")
    )

    @Bean
    fun lineRepository(dataSource: DataSource) = LineRepository(dataSource.lines)

    @Bean
    fun delayRepository(dataSource: DataSource) = DelayRepository(dataSource.delays)

    @Bean
    fun stopRepository(dataSource: DataSource) = StopRepository(dataSource.stops)

    @Bean
    fun timeRepository(dataSource: DataSource) = TimeRepository(dataSource.times)

}
