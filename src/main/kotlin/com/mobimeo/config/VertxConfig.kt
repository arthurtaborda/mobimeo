package com.mobimeo.config

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.mobimeo.api.ApiGateway
import io.vertx.core.Vertx
import io.vertx.core.json.Json
import io.vertx.core.json.Json.mapper
import org.springframework.context.annotation.Configuration
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.annotation.PostConstruct


@Configuration
class VertxConfig(val apiGateway: ApiGateway) {

    @PostConstruct
    fun deploy() {
        val vertx = Vertx.vertx()
        vertx.deployVerticle(apiGateway)

        Json.mapper.apply {
            registerKotlinModule()
            mapper.registerModule(JavaTimeModule().apply {
                this.addSerializer(
                    LocalTime::class.java,
                    LocalTimeSerializer(DateTimeFormatter.ISO_TIME)
                )
            })
        }
        Json.prettyMapper.apply {
            registerKotlinModule()
        }
    }
}

