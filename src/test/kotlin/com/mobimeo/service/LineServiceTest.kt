package com.mobimeo.service

import com.mobimeo.repository.CsvFile
import com.mobimeo.test.FakeClock
import com.mobimeo.repository.DataSource
import com.mobimeo.repository.DelayRepository
import com.mobimeo.repository.LineRepository
import com.mobimeo.repository.Location
import com.mobimeo.repository.StopRepository
import com.mobimeo.repository.TimeRepository
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.time.LocalTime
import java.util.stream.Stream

class LineServiceTest {

    val fakeClock = FakeClock()
    val dataSource = DataSource(
        CsvFile("datasource/lines.csv"),
        CsvFile("datasource/delays.csv"),
        CsvFile("datasource/stops.csv"),
        CsvFile("datasource/times.csv")
    )

    private val lineService = LineService(
        fakeClock,
        LineRepository(dataSource.lines),
        StopRepository(dataSource.stops),
        TimeRepository(dataSource.times),
        DelayRepository(dataSource.delays)
    )

    @ParameterizedTest
    @MethodSource("nextLines")
    fun `should get next line stopping after`(stopId: Int, time: LocalTime, expected: LineResponse) {
        fakeClock.time = time
        assertThat(lineService.findNextLineStoppingAt(stopId)).isEqualToComparingFieldByField(expected)
    }

    @ParameterizedTest
    @MethodSource("searchLines")
    fun `should search lines by time and location`(location: Location, time: LocalTime, expected: List<LineResponse>) {
        fakeClock.time = time
        assertThat(lineService.searchByTimeAndLocation(location, time)).containsExactlyInAnyOrderElementsOf(expected)
    }

    @Test
    fun `should throw exception when stop id does not exist`() {
        assertThatThrownBy { lineService.findNextLineStoppingAt(5) }
            .isInstanceOf(StopNotFoundException::class.java)
            .hasMessage("Stop 5 could not be found")
    }

    companion object {
        @JvmStatic
        fun nextLines() = Stream.of(
            Arguments.of(1, LocalTime.of(0, 0), LineResponse(1, "M1", LocalTime.of(10, 10), 1, 3)),
            Arguments.of(1, LocalTime.of(10, 9), LineResponse(1, "M1", LocalTime.of(10, 10), 1, 3)),
            Arguments.of(1, LocalTime.of(10, 10), LineResponse(1, "M1", LocalTime.of(10, 10), 1, 3)),
            Arguments.of(1, LocalTime.of(10, 11), LineResponse(2, "M3", LocalTime.of(10, 21), 1, null)),
            Arguments.of(1, LocalTime.of(10, 26), LineResponse(1, "M1", LocalTime.of(10, 10), 1, 3)),
            Arguments.of(2, LocalTime.of(23, 59), LineResponse(2, "M3", LocalTime.of(10, 13), 2, null)),
            Arguments.of(2, LocalTime.of(10, 24), LineResponse(1, "M1", LocalTime.of(10, 25), 2, 3)),
            Arguments.of(2, LocalTime.of(10, 10), LineResponse(2, "M3", LocalTime.of(10, 13), 2, null)),
            Arguments.of(2, LocalTime.of(10, 13), LineResponse(2, "M3", LocalTime.of(10, 13), 2, null)),
            Arguments.of(2, LocalTime.of(10, 26), LineResponse(2, "M3", LocalTime.of(10, 13), 2, null))
        )

        @JvmStatic
        fun searchLines() = Stream.of(
            Arguments.of(
                Location(4, 2), LocalTime.of(10, 5), listOf(
                    LineResponse(1, "M1", LocalTime.of(10, 5), 3, 3),
                    LineResponse(2, "M3", LocalTime.of(10, 5), 3, null)
                )
            ),
            Arguments.of(
                Location(1, 1), LocalTime.of(10, 10), listOf(LineResponse(1, "M1", LocalTime.of(10, 10), 1, 3))
            ),
            Arguments.of(
                Location(1, 1), LocalTime.of(10, 12), listOf<LineResponse>()
            ),
            Arguments.of(
                Location(1, 2), LocalTime.of(10, 10), listOf<LineResponse>()
            )
        )
    }
}
