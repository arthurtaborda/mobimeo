package com.mobimeo.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class StopRepositoryTest {

    @Test
    fun `should find by location`() {
        val stop1 = StopRecord(1, Location(1, 1))
        val stop2 = StopRecord(2, Location(2, 2))
        val stop3 = StopRecord(3, Location(2, 2))
        val stopRepository = StopRepository(setOf(stop1, stop2, stop3))
        assertThat(stopRepository.findByLocation(Location(1, 1))).containsExactlyInAnyOrder(stop1)
        assertThat(stopRepository.findByLocation(Location(2, 2))).containsExactlyInAnyOrder(stop2, stop3)
        assertThat(stopRepository.findByLocation(Location(3, 3))).isEmpty()
    }
}
