package com.mobimeo.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.time.LocalTime

class TimeRepositoryTest {

    @Test
    fun `should find first line by time of stop`() {
        val time1 = TimeRecord(1, 1, LocalTime.of(10, 10, 10))
        val time2 = TimeRecord(2, 1, LocalTime.of(10, 6, 10))
        val time3 = TimeRecord(3, 1, LocalTime.of(10, 8, 10))
        val timeRepository = TimeRepository(setOf(time1, time2, time3))
        assertThat(timeRepository.findFirstLineByTimeOfStop(1)).isEqualTo(time2)
        assertThat(timeRepository.findFirstLineByTimeOfStop(2)).isNull()
    }

    @Test
    fun `should find by stop id`() {
        val time1 = TimeRecord(1, 1, LocalTime.of(10, 10, 10))
        val time2 = TimeRecord(1, 2, LocalTime.of(10, 6, 10))
        val time3 = TimeRecord(1, 2, LocalTime.of(10, 8, 10))
        val timeRepository = TimeRepository(setOf(time1, time2, time3))
        assertThat(timeRepository.findByStopId(1)).containsExactlyInAnyOrder(time1)
        assertThat(timeRepository.findByStopId(2)).containsExactlyInAnyOrder(time2, time3)
        assertThat(timeRepository.findByStopId(3)).isEmpty()
    }

    @Test
    fun `should find by stop id and time`() {
        val time1 = TimeRecord(1, 1, LocalTime.of(10, 10, 10))
        val time2 = TimeRecord(1, 2, LocalTime.of(10, 6, 10))
        val time3 = TimeRecord(1, 3, LocalTime.of(10, 8, 10))
        val timeRepository = TimeRepository(setOf(time1, time2, time3))
        assertThat(timeRepository.findByStopIdAndTime(1, LocalTime.of(10, 10, 10))).containsExactlyInAnyOrder(time1)
        assertThat(timeRepository.findByStopIdAndTime(1, LocalTime.of(10, 6, 10))).isEmpty()
        assertThat(timeRepository.findByStopIdAndTime(2, LocalTime.of(10, 8, 10))).isEmpty()
    }
}
