package com.mobimeo.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class LineRepositoryTest {

    @Test
    fun `should find by line id`() {
        val line1 = LineRecord(1, "line1")
        val line2 = LineRecord(2, "line2")
        val line3 = LineRecord(3, "line3")
        val lineRepository = LineRepository(setOf(line1, line2, line3))
        assertThat(lineRepository.findByLineId(1)).isEqualTo(line1)
        assertThat(lineRepository.findByLineId(2)).isEqualTo(line2)
        assertThat(lineRepository.findByLineId(3)).isEqualTo(line3)
        assertThat(lineRepository.findByLineId(4)).isNull()
    }
}
