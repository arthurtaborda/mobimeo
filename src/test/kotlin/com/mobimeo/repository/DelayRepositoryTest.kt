package com.mobimeo.repository

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class DelayRepositoryTest {

    @Test
    fun `should find by line name`() {
        val delay1 = DelayRecord("line1", 1)
        val delay2 = DelayRecord("line2", 2)
        val delay3 = DelayRecord("line3", 3)
        val delayRepository = DelayRepository(setOf(delay1, delay2, delay3))
        assertThat(delayRepository.findByLineName("line1")).isEqualTo(delay1)
        assertThat(delayRepository.findByLineName("line2")).isEqualTo(delay2)
        assertThat(delayRepository.findByLineName("line3")).isEqualTo(delay3)
        assertThat(delayRepository.findByLineName("line4")).isNull()
    }
}
