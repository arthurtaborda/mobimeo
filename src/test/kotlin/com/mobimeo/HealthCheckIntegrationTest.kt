package com.mobimeo

import io.restassured.RestAssured.given
import org.junit.jupiter.api.Test

class HealthCheckIntegrationTest : IntegrationTest() {

    @Test
    fun contextLoads() {
        given()
            .`when`()
            .get("health")
            .then()
            .statusCode(200)
    }
}
