package com.mobimeo.api

import com.mobimeo.IntegrationTest
import com.mobimeo.test.FakeClock
import io.restassured.RestAssured.given
import io.restassured.http.ContentType.JSON
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.nullValue
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalTime

class ApiIntegrationTest : IntegrationTest() {

    @Nested
    inner class FindNextLine : IntegrationTest() {

        @Autowired
        lateinit var fakeClock: FakeClock

        @Test
        fun `should return next bus coming in stop`() {
            fakeClock.time = LocalTime.of(10, 10)

            given()
                .contentType(JSON)
                .queryParam("stop", "2")
                .get("/api/lines/next")
                .then()
                .statusCode(200)
                .body("id", equalTo(2))
                .body("name", equalTo("M3"))
                .body("stopsAt", equalTo("10:13:00"))
                .body("stopId", equalTo(2))
                .body("delayInMinutes", nullValue())
        }

        @Test
        fun `should return bad request when stop id does not exist`() {
            fakeClock.time = LocalTime.of(10, 10)

            given()
                .contentType(JSON)
                .queryParam("stop", "10")
                .get("/api/lines/next")
                .then()
                .statusCode(400)
                .body("message", equalTo("Stop 10 could not be found"))
        }

        @Test
        fun `should return bad request when missing query param`() {
            fakeClock.time = LocalTime.of(10, 10)

            given()
                .contentType(JSON)
                .get("/api/lines/next")
                .then()
                .statusCode(400)
                .body("message", equalTo("Missing 'stop' query param"))
        }
    }

    @Nested
    inner class SearchLine : IntegrationTest() {

        @Autowired
        lateinit var fakeClock: FakeClock

        @Test
        fun `should search line by time and location`() {
            fakeClock.time = LocalTime.of(10, 10)

            given()
                .contentType(JSON)
                .queryParam("time", "10:05:00")
                .queryParam("x", "4")
                .queryParam("y", "2")
                .get("/api/lines/search")
                .then()
                .statusCode(200)
                .body("size()", `is`(2))
                .body("[0].id", equalTo(1))
                .body("[0].name", equalTo("M1"))
                .body("[0].stopsAt", equalTo("10:05:00"))
                .body("[0].stopId", equalTo(3))
                .body("[0].delayInMinutes", equalTo(3))
                .body("[1].id", equalTo(2))
                .body("[1].name", equalTo("M3"))
                .body("[1].stopsAt", equalTo("10:05:00"))
                .body("[1].stopId", equalTo(3))
                .body("[1].delayInMinutes", nullValue())
        }

        @Test
        fun `should return bad request when missing query param`() {
            fakeClock.time = LocalTime.of(10, 10)

            given()
                .contentType(JSON)
                .queryParam("time", "10:05:00")
                .queryParam("y", "2")
                .get("/api/lines/search")
                .then()
                .statusCode(400)
                .body("message", equalTo("Missing 'x' query param"))
        }
    }

}
