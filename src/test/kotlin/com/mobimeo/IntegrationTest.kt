package com.mobimeo

import com.mobimeo.repository.CsvFile
import com.mobimeo.test.FakeClock
import com.mobimeo.repository.DataSource
import com.mobimeo.test.RestAssuredExtension
import io.restassured.RestAssured
import io.restassured.parsing.Parser
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension

@ExtendWith(SpringExtension::class, RestAssuredExtension::class)
@SpringBootTest(classes = [IntegrationTestConfig::class])
@ActiveProfiles("test")
abstract class IntegrationTest

@Configuration
@Import(Launcher::class)
class IntegrationTestConfig {

    @Bean
    @Primary
    fun clock() = FakeClock()

    @Bean
    @Primary
    fun dataSource() = DataSource(
        CsvFile("datasource/lines.csv"),
        CsvFile("datasource/delays.csv"),
        CsvFile("datasource/stops.csv"),
        CsvFile("datasource/times.csv")
    )
}

