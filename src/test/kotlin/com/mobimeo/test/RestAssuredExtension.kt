package com.mobimeo.test

import io.restassured.RestAssured
import io.restassured.parsing.Parser
import org.junit.jupiter.api.extension.BeforeAllCallback
import org.junit.jupiter.api.extension.ExtensionContext


class RestAssuredExtension : BeforeAllCallback {

    override fun beforeAll(context: ExtensionContext) {
        RestAssured.port = 8081
        RestAssured.defaultParser = Parser.JSON
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()
    }
}
