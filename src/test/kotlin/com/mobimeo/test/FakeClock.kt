package com.mobimeo.test

import java.time.Clock
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.ZoneOffset

class FakeClock : Clock() {

    var date = LocalDate.now()
    var time = LocalTime.now()

    override fun withZone(zone: ZoneId?) = this

    override fun getZone() = ZoneOffset.UTC

    override fun instant() = LocalDateTime.of(date, time).atZone(ZoneOffset.UTC).toInstant()
}
