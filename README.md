## Mobimeo Code Challenge

### Design decisions

- In order to have a cleaner code I have decided to not take into account the time complexity of the algorithms. It's extremely unoptimized to keep the simplicity of the solution
- Using Spring Boot for the dependency injection mostly, decided to go for vertx as the web server because of my preference on more explicit code (less "magic")
- There are some additional methods for logging: `genInfo`, `genError` and `genWarn`. I like to use them to keep a format consistency across the codebase. It is helpful to index values in a log search tool.

### How to run:

````
./gradlew bootRun
````


### How to run tests:

````
./gradlew test
````

The tests reports are located in `build/reports/tests/test/index.html`

